import { setTrackers, getData } from './functions'

let btns = document.querySelectorAll('button')
let hobbies = document.querySelector('.hobbies')
let data

getData((datas) => {
    data = datas

    window.addEventListener('DOMContentLoaded', setTrackers(document.querySelector('.active'), data, hobbies))
})

btns.forEach((btn) => {
    btn.addEventListener('click', () => {
        btns.forEach((btn) => btn.classList.remove('active'))
        btn.classList.add('active')
        console.log(data)
        setTrackers(btn, data, hobbies)
    })
})
