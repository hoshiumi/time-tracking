function setTrackers(params, data, receiver) {
    receiver.innerHTML = ''
    let periodicity = params.attributes[0].value

    let test = periodicity === 'weekly' ? 'Last week' : periodicity === 'daily' ? 'Yesterday' : 'Last month'
    console.log(test)

    data = data.map((d) => {
        console.log(d.timeframes[periodicity])
        receiver.innerHTML += `
                <div class="hobbies_card">
                    <div class="img"></div>
                    <div class="content">
                        <div class="header">
                            <h2 class="title">${d.title}</h2>
                            <span>
                                <img src="./images/icon-ellipsis.svg" alt="" />
                            </span>
                        </div>
                        <div class="value">
                            <h2 class="time">${d.timeframes[periodicity].current}hrs</h2>
                            <p class="description">${test} - <span>${d.timeframes[periodicity].previous}hrs</span></p>
                        </div>
                    </div>
                </div>
            `
    })
}

function getData(callback) {
    let xhr = new XMLHttpRequest()
    let result

    xhr.overrideMimeType('application/json')
    xhr.open('GET', '/data.json', true)

    xhr.onload = () => {
        result = JSON.parse(xhr.responseText)
        callback(result)
    }

    xhr.send()
}

export { setTrackers, getData }
